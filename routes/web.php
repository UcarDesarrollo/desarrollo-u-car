<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('prueba', function () {
    return view('prueba');
})->name('prueba');

Route::get('gerente', 'AdminController@inicio')->name('home');

Route::get('gerente/inicio', 'AdminController@inicioGerente')->name('homeG');

Route::get('gerente/usuarios/empleados/administradores/inicio', 'AdminController@altaAdmin')->name('alta_Admin');

Route::get('gerente/usuarios/empleados/chofer/alta_chofer', 'AdminController@nuevoChofer')->name('chofer');

Route::get('gerente/usuarios/vehiculo/alta_vehiculo','AdminController@Vehiculo')->name('vehiculo');

Route::get('gerente/usuarios/sucursal/alta_sucursal','AdminController@Sucursal')->name('sucursal');


Route::get('/', function () {
    return view('index');
})->name('index') ;

Route::get('Reservacion', function () {
    return view('Reservacion');
})->name('Reserva') ;

Route::get('flota', function () {
    return view('flota');
})->name('flota') ;

Route::get('inicio_sesion_cliente', function () {
    return view('inicio_sesion_cliente');
})->name('inicio_sesion_cliente') ;

Route::get('servicios', function () {
    return view('servicios');
})->name('servicios') ;

Route::get('sucursales', function () {
    return view('sucursales');
})->name('sucursales') ;

Route::get('altaChofer', function () {
    return view('alta_chofer');
})->name('altaChofer') ;

