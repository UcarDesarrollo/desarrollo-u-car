@extends("theme.$theme.layout")
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Alta vehiculo</title>
  <script src="https://ajax.googleapis.com/ajax/libs/d3js/5.9.0/d3.min.js"></script>
 
</head>
<body>
  
@section('contenido')
    <section class="content-header">
        <h1>
          Panel de administracion |
          <small>Gerente</small>
        </h1>
        
    </section>


    <section class="content">
        <div class="col-md-12" style="margin-top: 2%;">
            <div class="box box-primary">             
                <div class="box-header with-border">
                  <h3 class="box-title">Nuevo Vehiculo</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->


                <!-- FORM PARA PREVISUALIZAR FOTO -->
              <form role="form">
                  <div class="box-body">           
                                                        
                                       
                    <div class="col-md-6">
                     
                        <div class="col-md-6 form-group">
                            <label>Número VIN</label>
                          <input type="text" name="vin" id="" class="form-control" autofocus>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Matricula</label>
                          <input type="text" name="matricula" id="" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                          <label>Marca</label>
                          <input type="text" name="matricula" id="" class="form-control">
                        </div>

                        <div class="col-md-6 form-group">
                            <label>Modelo</label>
                            <input type="text" name="modelo" id="" class="form-control">
                          </div>

                          <div class="col-md-6 form-group">
                              <label>Año</label>
                              <input type="text" name="año" id="" class="form-control">
                            </div>

                            <div class="col-md-6 form-group">
                                <label>Rendimiento</label>
                                <input type="text" name="rendimiento" id="" class="form-control">
                              </div>
                            
                              <div class="col-md-6 form-group">
                                  <label>Color</label>
                                  <input type="text" name="color" id="" class="form-control">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label>Número de pasajeros</label>
                                    <input type="text" name="pasajeros" id="" class="form-control">
                                  </div>

                                  <div class="col-md-6 form-group">
                                      <label>Maletero</label>
                                      <input type="text" name="maletero" id="" class="form-control">
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <label>Cilindros</label>
                                        <input type="text" name="cilindros" id="" class="form-control">
                                      </div>
                                      
                                      <div class="col-md-6 form-group">
                                          <label>Kilometraje</label>
                                          <input type="text" name="kilometraje" id="" class="form-control">
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label>Status</label>
                                            <input type="text" name="status" id="" class="form-control">
                                          </div>

                                            <div class="col-md-6 form-group">
                                              <label>Precio compra</label>
                                              <input type="text" name="costo" id="" class="form-control">
                                            </div>

                                            <div class="col-md-6 form-group">
                                                <label>Precio renta</label>
                                                <input type="text" name="precio" id="" class="form-control">
                                              </div>


                    </div>

                    <div class="col-md-6">
                        <div id="preview" style="margin-top: 5%;">
                         <img src="http://www.cespcampeche.gob.mx/repuve/images/chip-blanco-2.png" style="width: 500px; height: 400px;" >
                        </div>
                        <hr>
                        <div class="col-md-1 col-md-offset-5  file-loading">
                        <span class="btn btn-primary btn-file"> Subir Foto
                        <input id="file" type="file"/>    </span>  
                        </div> 
                  </div> 
                   
                    
              
                    

                  <!-- /.box-body -->
                  <div class="row">
                    <div class="col-md-12">
                        <div class="box-footer" style="float: right">
                            <button type="submit" class="btn btn-primary">Agregar</button>
                          </div>
                        <div class="box-footer" style="float: right">
                            <button type="submit" class="btn btn-danger">Cancelar</button>
                          </div>                       
                      </div>                    
                  </div>
                  
                </form>
              </div>
         
         </div>
    </section>
   

@endsection


@section('scripts')
<script>
document.getElementById("file").onchange = function(e) {
  // Creamos el objeto de la clase FileReader
  let reader = new FileReader();

  // Leemos el archivo subido y se lo pasamos a nuestro fileReader
  reader.readAsDataURL(e.target.files[0]);

  // Le decimos que cuando este listo ejecute el código interno
  reader.onload = function(){
    let preview = document.getElementById('preview'),
            image = document.createElement('img');

    image.src = reader.result;
    image.getElementsByClassName('rounded-circle');
    image.style.width="500px";
    image.style.height="400px";
    preview.innerHTML = '';
    preview.append(image);
  };
}
</script>
@endsection

                  

</body>
</html>
  
  
